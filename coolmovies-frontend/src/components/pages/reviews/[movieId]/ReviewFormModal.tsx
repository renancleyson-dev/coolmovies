import React, { ChangeEvent, FC, FormEvent, useEffect, useState } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormLabel,
  Input,
  Rating,
  TextareaAutosize,
  Typography,
} from '@mui/material';
import { css } from '@emotion/react';
import { useRouter } from 'next/router';
import { red } from '@mui/material/colors';
import { movieReviewsActions, useAppDispatch, useAppSelector } from '../../../../redux';

type Props = {
  reviewId?: string;
  open: boolean;
  onClose: () => void;
};

type Form = {
  title: string;
  body: string;
  rating: number;
};

const initialValues = { title: '', body: '', rating: 0 };

const ReviewFormModal: FC<Props> = ({ reviewId, open, onClose }) => {
  const dispatch = useAppDispatch();
  const { movieId } = useRouter().query as { movieId: string };
  const userReviewerId = useAppSelector((state) => state.auth.user?.id);
  const [form, setForm] = useState<Form>(initialValues);
  const { upsert, list: movieReviews } = useAppSelector((state) => state.movieReviews);

  useEffect(() => {
    if (open) {
      if (reviewId) {
        const review = movieReviews.find((review) => review.id === reviewId);

        if (review) {
          const { rating, title, body } = review;
          setForm({ rating, title, body });
        }
      } else {
        setForm(initialValues);
      }
    }
  }, [reviewId, open, movieReviews]);

  useEffect(() => {
    if (upsert.isSuccessful) {
      dispatch(movieReviewsActions.fetch({ movieId }));
      onClose();
    }
  }, [upsert.isSuccessful, dispatch, onClose, movieId]);

  const isValid = Object.values(form).every((value) => !!value);

  const onChangeInput =
    (field: keyof Form) => (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) =>
      setForm((state) => ({ ...state, [field]: e.target.value }));

  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    dispatch(
      movieReviewsActions.create({
        data: { ...form, id: reviewId, movieId, userReviewerId: userReviewerId! },
      })
    );
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>{'Review this movie'}</DialogTitle>
      <DialogContent>
        <form onSubmit={onSubmit} id={'review-form'}>
          <FormControl css={styles.formControl} fullWidth required>
            <FormLabel>{'Give the movie a rating'}</FormLabel>
            <Rating
              value={form.rating}
              onChange={(event, newValue) => {
                if (newValue !== null) {
                  setForm((state) => ({ ...state, rating: newValue }));
                }
              }}
            />
          </FormControl>
          <FormControl css={styles.formControl} fullWidth required>
            <FormLabel htmlFor='title-input'>{'Title'}</FormLabel>
            <Input
              id={'title-input'}
              value={form.title}
              onChange={onChangeInput('title')}
              placeholder='Write a title...'
            />
          </FormControl>
          <FormControl css={styles.formControl} fullWidth required>
            <FormLabel htmlFor='description-input'>{'Description'}</FormLabel>
            <TextareaAutosize
              id={'description-input'}
              value={form.body}
              onChange={onChangeInput('body')}
              placeholder='Write more about it...'
              minRows={5}
              css={styles.descriptionInput}
            />
          </FormControl>
          {upsert.error && <Typography css={styles.errorText}>{upsert.error.message}</Typography>}
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} disabled={upsert.isLoading}>
          Cancel
        </Button>
        <Button
          type={'submit'}
          form={'review-form'}
          variant={'contained'}
          disabled={!isValid || upsert.isLoading}
        >
          {upsert.isLoading ? 'Loading...' : 'Save'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const styles = {
  errorText: css({ color: red[500] }),
  descriptionInput: css({ maxWidth: '100%' }),
  formControl: css({
    ':not(:last-of-type)': {
      marginBottom: '20px',
    },
  }),
};

export default ReviewFormModal;
