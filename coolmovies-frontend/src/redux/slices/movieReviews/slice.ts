import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type BaseMovieReview = {
  id?: string;
  title: string;
  body: string;
  rating: number;
  movieId: string;
  userReviewerId: string;
};

// Maybe I will use the GraphQL codegen for types later if I find some time.
export type MovieReviewView = Pick<BaseMovieReview, 'body' | 'title' | 'rating'> & {
  id: string;
  userByUserReviewerId: { name: string };
};

export type CurrentMovie = { title: string; imgUrl: string };

interface MoviesState {
  list: MovieReviewView[];
  currentMovie: CurrentMovie | null;
  isLoading: boolean;
  error: { message: string } | null;
  upsert: {
    isSuccessful: boolean;
    isLoading: boolean;
    error: { message: string } | null;
  };
}

const initialState: MoviesState = {
  list: [],
  isLoading: false,
  error: null,
  currentMovie: null,
  upsert: {
    isSuccessful: false,
    isLoading: false,
    error: null,
  },
};

export const slice = createSlice({
  initialState,
  name: 'movieReviews',
  reducers: {
    fetch: (state, action: PayloadAction<{ movieId: string }>) => {
      state.list = [];
      state.isLoading = true;
    },
    loaded: (
      state,
      action: PayloadAction<{
        data: MovieReviewView[];
        currentMovie: CurrentMovie;
      }>
    ) => {
      state.list = action.payload.data;
      state.currentMovie = action.payload.currentMovie;
      state.isLoading = false;
    },
    loadError: (state) => {
      state.error = { message: 'Something went wrong' };
      state.isLoading = false;
    },
    create: (state, action: PayloadAction<{ data: BaseMovieReview }>) => {
      state.upsert.isLoading = true;
      state.upsert.isSuccessful = false;
    },
    createSuccess: (state) => {
      state.upsert.isLoading = false;
      state.upsert.isSuccessful = true;
    },
    createFail: (state) => {
      state.upsert.error = { message: 'Something went wrong' };
      state.upsert.isLoading = false;
    },
    clearData: () => {
      return initialState;
    },
  },
});

export const { actions } = slice;
export type SliceAction = typeof actions;
export default slice.reducer;
