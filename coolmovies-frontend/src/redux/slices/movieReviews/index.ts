import { combineEpics } from 'redux-observable';
export { actions as movieReviewsActions } from './slice';
export { default as movieReviewsReducer } from './slice';
import { fetchMovieReviews, saveMovieReview } from './epics';

export const movieReviewsEpics = combineEpics(fetchMovieReviews, saveMovieReview);
