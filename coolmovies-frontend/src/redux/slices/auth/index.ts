
import { combineEpics } from 'redux-observable';
export { actions as authActions } from './slice';
export { default as authReducer } from './slice';
import { fetchUser } from './epics';

export const authEpics = combineEpics(fetchUser);
