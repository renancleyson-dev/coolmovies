/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState, useCallback } from 'react';
import { css } from '@emotion/react';
import type { NextPage } from 'next';
import { useRouter } from 'next/router';
import { Box, Button, Skeleton, Typography } from '@mui/material';
import { movieReviewsActions, useAppDispatch, useAppSelector } from '../../redux';
import MovieReviewsList from '../../components/pages/reviews/[movieId]/MovieReviewsList';
import ReviewFormModal from '../../components/pages/reviews/[movieId]/ReviewFormModal';

const MovieReviews: NextPage = () => {
  const [isOpen, setOpen] = useState(false);
  const [editId, setEditId] = useState<string>();
  const { movieId } = useRouter().query as { movieId: string };

  const { list, isLoading, error, currentMovie } = useAppSelector((state) => state.movieReviews);
  const dispatch = useAppDispatch();

  const onClose = useCallback(() => {
    setOpen(false);
    setEditId(undefined);
  }, []);

  useEffect(() => {
    if (movieId) {
      dispatch(movieReviewsActions.fetch({ movieId }));
    }
  }, [dispatch, movieId]);

  const onOpen = () => setOpen(true);

  const editReview = (reviewId: string) => {
    setEditId(reviewId);
    onOpen();
  };

  if (error) {
    return <Typography variant={'h1'}>{error.message}</Typography>;
  }

  return (
    <>
      <Box css={styles.movieContainer}>
        {currentMovie ? (
          /*
            Only nextJS 12.2 has support for wildcards to allow any domain
            for the next's Image component so lets use an img tag
          */
          <img
            src={currentMovie.imgUrl}
            alt={`${currentMovie.title} image`}
            css={styles.movieImage}
          />
        ) : (
          <Skeleton variant={'rectangular'} width={'400px'} height={'60vh'} />
        )}
        <Box css={styles.movieContent}>
          <Box css={styles.movieHeading}>
            <Box>
              {currentMovie ? (
                <Typography variant={'h2'}>{currentMovie.title}</Typography>
              ) : (
                <Skeleton width={'20vw'} height={'50px'} />
              )}
              {currentMovie ? (
                <Typography
                  css={styles.movieSubtitle}
                  variant={'subtitle1'}
                >{`Add and view reviews of the ${currentMovie.title}`}</Typography>
              ) : (
                <Skeleton width={'30vw'} height={'80px'} />
              )}
            </Box>
            <Button css={styles.reviewButton} variant={'contained'} onClick={onOpen}>
              Add review
            </Button>
            <ReviewFormModal reviewId={editId} open={isOpen} onClose={onClose} />
          </Box>
          <MovieReviewsList
            editReview={editReview}
            movieReviews={isLoading ? new Array(1).fill(undefined) : list.slice(0, 1)}
          />
        </Box>
      </Box>
      {isLoading || list.length > 1 ? (
        <>
          <Typography variant={'h5'} css={styles.moreReviewHeading}>
            Other reviews
          </Typography>
          <MovieReviewsList
            editReview={editReview}
            movieReviews={isLoading ? new Array(6).fill(undefined) : list.slice(1)}
          />
        </>
      ) : null}
    </>
  );
};

const styles = {
  movieContainer: css({
    display: 'flex',
    alignSelf: 'stretch',
    flexWrap: 'wrap',
  }),
  movieContent: css({
    marginLeft: '40px',
    flex: 1,
    '@media (max-width: 929px)': {
      marginTop: '50px',
      marginLeft: '0',
      minWidth: '100%',
    },
  }),
  movieHeading: css({
    display: 'flex',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  }),
  movieSubtitle: css({
    textAlign: 'start',
    marginBottom: '15px',
  }),
  reviewButton: css({ width: '128px' }),
  movieImage: css({
    width: '400px',
    objectFit: 'contain',
    objectPosition: 'top',
    height: '70vh',
    '@media (max-width: 929px)': {
      width: 'auto',
      maxWidth: '100%',
      flex: '1 1',
    },
    '@media (max-width: 490px)': {
      height: 'auto',
    },
  }),
  moreReviewHeading: css({
    textAlign: 'start',
    marginTop: '60px',
    alignSelf: 'stretch',
  }),
};

export default MovieReviews;
