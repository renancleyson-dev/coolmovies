import { gql } from '@apollo/client';
import { Epic, StateObservable } from 'redux-observable';
import { Observable } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { RootState } from '../../store';
import { EpicDependencies } from '../../types';
import { actions, User, SliceAction } from './slice';

const USER_STORAGE_KEY = 'USER_STORAGE_KEY';

const getLocalUser = (): User | undefined | null => {
  const user = localStorage.getItem(USER_STORAGE_KEY);

  if (user !== null) {
    return JSON.parse(user);
  }

  return user;
};
const saveUser = (user: User) =>
  localStorage.setItem(USER_STORAGE_KEY, JSON.stringify(user));

export const fetchUser: Epic = (
  action$: Observable<SliceAction['fetch']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetch.match),
    switchMap(async () => {
      const userStorage = getLocalUser();

      if (userStorage === undefined) {
        return;
      }
      if (userStorage !== null) {
        return actions.loaded({ data: userStorage });
      }

      try {
        const result: { data: { currentUser: User } } = await client.query({
          query: CurrentUserQuery,
        });

        saveUser(result.data.currentUser);

        return actions.loaded({ data: result.data.currentUser });
      } catch (err) {
        return actions.loadError();
      }
    })
  );

const CurrentUserQuery = gql`
  query {
    currentUser {
      id
      name
    }
  }
`;
