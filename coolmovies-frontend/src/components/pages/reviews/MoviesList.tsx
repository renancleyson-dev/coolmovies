import React, { FC } from 'react';
import Link from 'next/link';
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Skeleton,
  Typography,
  useMediaQuery,
} from '@mui/material';
import { css } from '@emotion/react';
import { Movie } from '../../../redux/slices/movies/slice';

const MoviesList: FC<{
  movies: (Movie | undefined)[];
}> = ({ movies }) => {
  const isLargerThan490 = useMediaQuery('@media (min-width: 490px)');

  const list = movies.map((item, index) => (
    <Grid key={item?.id || index} item xs={isLargerThan490 ? 3 : 12} css={styles.cardContainer}>
      <Card>
        {item ? (
          <Link href={`/reviews/${item.id}`} passHref>
            <CardActionArea component={'a'}>
              <CardMedia
                src={item.imgUrl}
                alt={`${item.title} image`}
                component={'img'}
                css={styles.image}
              />
              <CardContent>
                <Typography variant={'h6'} component={'div'}>
                  {item.title}
                </Typography>
              </CardContent>
            </CardActionArea>
          </Link>
        ) : (
          <>
            <Skeleton variant={'rectangular'} width={'100%'} height={'450px'} />
            <CardContent>
              <Skeleton width={'40%'} />
              <Skeleton width={'70%'} />
            </CardContent>
          </>
        )}
      </Card>
    </Grid>
  ));

  if (list.length === 0) {
    return <Typography>No movies were found available.</Typography>;
  }

  return (
    <Grid container css={styles.listContainer} spacing={5}>
      {list}
    </Grid>
  );
};

const styles = {
  listContainer: css({
    marginTop: '30px',
    width: '90%',
    justifyContent: 'center',
  }),
  image: css({
    width: '100%',
    maxHeight: '60vh',
    '@media (max-width: 490px)': {
      height: 'auto',
    },
  }),
  cardContainer: css({
    '@media (min-width: 490px)': {
      minWidth: '280px',
    },
  }),
};

export default MoviesList;
