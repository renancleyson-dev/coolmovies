import { gql } from '@apollo/client';
import { Epic, StateObservable } from 'redux-observable';
import { Observable } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { RootState } from '../../store';
import { EpicDependencies } from '../../types';
import { actions, Movie, SliceAction } from './slice';

export const fetchMovies: Epic = (
  action$: Observable<SliceAction['fetch']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetch.match),
    switchMap(async () => {
      try {
        const result: { data: { allMovies: { nodes: Movie[] } } } =
          await client.query({
            query: AllMoviesQuery,
          });

        return actions.loaded({ data: result.data.allMovies.nodes });
      } catch (err) {
        return actions.loadError();
      }
    })
  );

const AllMoviesQuery = gql`
  query AllMovies {
    allMovies {
      nodes {
        id
        imgUrl
        title
      }
    }
  }
`;
