import React, { useEffect } from 'react';
import type { NextPage } from 'next';
import { css } from '@emotion/react';
import { Typography } from '@mui/material';
import { yellow } from '@mui/material/colors';
import MoviesList from '../../components/pages/reviews/MoviesList';
import { moviesActions, useAppDispatch, useAppSelector } from '../../redux';
const Reviews: NextPage = () => {
  const { list, isLoading } = useAppSelector((state) => state.movies);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(moviesActions.fetch());
  }, [dispatch]);

  return (
    <>
      <Typography variant={'h1'}>{'Reviews'}</Typography>
      <Typography variant={'subtitle1'}>
        {'Select a movie to view its reviews'}
      </Typography>
      {<MoviesList movies={isLoading ? new Array(4).fill(undefined) : list} />}
    </>
  );
};

export default Reviews;
