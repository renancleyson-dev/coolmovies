import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Maybe I will use the GraphQL codegen for types later if I find some time.
export type Movie = {
  id: string;
  imgUrl: string;
  title: string;
};

interface MoviesState {
  list: Movie[];
  isLoading: boolean;
  error: { message: string } | null;
}

const initialState: MoviesState = {
  list: [],
  isLoading: false,
  error: null,
};

export const slice = createSlice({
  initialState,
  name: 'movies',
  reducers: {
    fetch: (state) => {
      state.list = [];
      state.isLoading = true;
    },
    clearData: () => {
      return initialState;
    },
    loaded: (state, action: PayloadAction<{ data: Movie[] }>) => {
      state.list = action.payload.data;
      state.isLoading = false;
    },
    loadError: (state) => {
      state.error = { message: 'Something went wrong' };
      state.isLoading = false;
    },
  },
});

export const { actions } = slice;
export type SliceAction = typeof actions;
export default slice.reducer;
