import type { AppProps } from 'next/app';
import React, { FC } from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import Head from 'next/head';
import { createStore } from '../redux';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { CssBaseline, ThemeProvider } from '@mui/material';
import BaseLayout from '../components/layouts/BaseLayout';
import theme from '../theme';

const client = new ApolloClient({
  cache: new InMemoryCache(),
  uri: '/graphql',
});

const store = createStore({ epicDependencies: { client } });

const App: FC<AppProps> = ({ Component, pageProps }) => (
  <>
    <Head>
      <title>{'Coolmovies Frontend'}</title>
      <meta charSet='UTF-8' />
      <meta httpEquiv='X-UA-Compatible' content='IE=edge' />
      <meta name='viewport' content='width=device-width, initial-scale=1.0' />
    </Head>
    <ThemeProvider theme={theme}>
      <ReduxProvider store={store}>
        <CssBaseline />
        <BaseLayout>
          <Component {...pageProps} />
        </BaseLayout>
      </ReduxProvider>
    </ThemeProvider>
  </>
);

export default App;
