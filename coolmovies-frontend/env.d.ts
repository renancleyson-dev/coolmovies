declare global {
  namespace NodeJS {
    interface ProcessEnv extends NodeJS.ProcessEnv {
      // Don't forget to use ? mark for optional env variables
      GRAPHQL_URL: string;
    }
  }
}

export {};
