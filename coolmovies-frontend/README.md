**Read my [candidate's notes](#candidate-notes) right below!**

# Coolmovies web challenge

You have to add the cool movies review feature to the existing `coolmovies-frontend`.

We have created a basic app for you to get started in.

What tooling has been setup for you:

- [Next.js](https://nextjs.org/) (Build Framework)
- [MUI](https://mui.com/) (Component Library)
- [Redux Toolkit](https://redux-toolkit.js.org/) (State Management)
- [Redux-Observable](https://redux-observable.js.org/) (State Side-effect Middleware)
- [Apollo GraphQL](https://www.apollographql.com/) (GraphQL Query Client)

You must use these tools to complete the test. If you're unfamiliar with any of these, please read their documentation. We have also added some example code for the ideal patterns we would like to see. Have a look at `pages/index.tsx`.

We are providing you a GraphQL API mock application to consume.

## Acceptance Criteria

**You will be evaluated on your UI/UX. We expect this to be at the level of a basic prototype; clean and clear flow.**

You will be evaluated against your ability to understand and use the tooling provided and mimic existing patterns that are shown in the examples.

There are 3 main components for this feature, they **MUST** be completed and working:

1. Listing of the movie reviews.
2. Editing the existing movie reviews.
3. Adding additional reviews.

Additional things we would like to see, you **must do at least 5** of these:

1. The feature is available on the `/reviews` endpoint of the application.
2. The design is responsive.
3. Our designers don't like the default MUI blue. Change this.
4. Make the proxied GraphQL URL an environment variable.
5. Improve the folder structure of the frontend application how you see fit. (It's intentionally not great)
6. Add the custom `edit.svg` from the `public` folder as an icon to launch editing the review.
7. Add a unit testing framework of your choice, and some unit tests around the more complex areas of your code.

## Candidate Notes

I would like to thank y'all from the ecoPortal team for considering my application and this test. I'm very excited about the role!

### New folder structure

| Folder | Description |
| ------ | ----------- |
| `src/` | The source code of the application. It keeps the root directory simple as it will only contain the base configuration |
| `src/components/` | Components for the application that are divided in subdirectories, there's the `pages/` folder for components of specific pages, `shared/` folder for components shared through the application, and the `layouts/` folder(the name is pretty straightforward). New subdirectories may be added if needed. |
| `src/redux/` | Same as before, here's the slices, epics, redux setup, and anything else related to redux. |
| `src/utils/` | Since this folder can possibly become hard to organize, let's keep it divided into a `pages/` and `shared/` subdirectories. |

Now here's the folder structure in a tree-like format:
```
src/
|___components
    |___layouts
    |___pages
    |___shared
|___pages
|___redux
    |___slices
        |___example
            |___epics
|___utils
    |___pages
    |___shared
```

### Additional things I done(It's more than what was listed above):

1. The feature is available on the `/reviews` endpoint of the application. :white_check_mark:
2. The design is responsive. :white_check_mark:
3. Our designers don't like the default MUI blue. Change this. :white_check_mark:
4. Make the proxied GraphQL URL an environment variable. :white_check_mark:
5. Improve the folder structure of the frontend application how you see fit. (It's intentionally not great) :white_check_mark:
6. Add the custom `edit.svg` from the `public` folder as an icon to launch editing the review. :white_check_mark:
7. Add a unit testing framework of your choice, and some unit tests around the more complex areas of your code. :x:
   1. I didn't had the time for this one but I have some side projects to show my skills with unit/integration testing in React like [toggl-clone](https://github.com/renancleyson-dev/toggl-clone-frontend/tree/55a4b5ed5771658378ae6fa690d7117532bb61ca/src/__tests__) and [world-grid](https://github.com/renancleyson-dev/world-grid/tree/main/src/__tests__).
8. Add type safety to env variables. :heavy_plus_sign:
9.  Change CSS resetting from `global.css` file to MUI's component `CssBaseline`. :heavy_plus_sign:
10. Add editorconfig. :heavy_plus_sign:
