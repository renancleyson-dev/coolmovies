import { css } from '@emotion/react';
import { Box, CircularProgress, Paper, Typography } from '@mui/material';
import { FC, PropsWithChildren, useEffect } from 'react';
import { authActions, useAppDispatch, useAppSelector } from '../../redux';

const BaseLayout: FC<PropsWithChildren<{}>> = ({ children }) => {
  const dispatch = useAppDispatch();
  const { user, error, isLoading } = useAppSelector((state) => state.auth);

  useEffect(() => {
    dispatch(authActions.fetch());
  }, [dispatch]);

  if (isLoading) {
    return (
      <Box css={styles.loadingScreen}>
        <CircularProgress css={styles.loadingProgress} />
        Loading...
      </Box>
    );
  }

  if (error) {
    return <Box css={styles.loadingScreen}>{error.message}</Box>;
  }

  return (
    <Box css={styles.root}>
      <Paper
        elevation={3}
        css={styles.navBar}
        sx={({ palette }) => ({ backgroundColor: palette.primary.main })}
      >
        <Typography>{'EcoPortal'}</Typography>
        <Typography marginLeft="auto">Welcome {user?.name}!</Typography>
      </Paper>

      <Box css={styles.body}>{children}</Box>
    </Box>
  );
};

const styles = {
  root: css({
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  }),
  navBar: css({
    height: 50,
    alignSelf: 'stretch',
    display: 'flex',
    alignItems: 'center',
    padding: '16px 40px',
    borderRadius: 0,
    p: {
      color: 'white',
    },
  }),
  body: css({
    alignSelf: 'stretch',
    padding: '48px 40px 40px 40px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  }),
  loadingProgress: css({ marginRight: '12px' }),
  loadingScreen: css({
    display: 'flex',
    height: '100vh',
    width: '100vw',
    justifyContent: 'center',
    alignItems: 'center',
  }),
};

export default BaseLayout;
