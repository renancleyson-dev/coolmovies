import { createTheme } from '@mui/material';

const theme = createTheme({
  palette: {
    primary: {
      main: '#1976d2',
    },
    secondary: {
      main: '#53C351',
    },
  },
  typography: {
    h1: {
      fontSize: '2.75rem',
      textAlign: 'center',
    },
    h2: {
      fontSize: '1.8rem',
    },
    subtitle1: {
      fontWeight: 300,
      textAlign: 'center',
      color: 'rgba(0, 0, 0, 0.6)',
    },
  },
});

export default theme;
