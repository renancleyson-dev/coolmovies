import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Maybe I will use the GraphQL codegen for types later if I find some time.
export type User = {
  id: string;
  name: string;
};

interface AuthState {
  user: User | null;
  isLoading: boolean;
  error: { message: string } | null;
}

const initialState: AuthState = {
  user: null,
  isLoading: false,
  error: null,
};

export const slice = createSlice({
  initialState,
  name: 'auth',
  reducers: {
    fetch: (state) => {
      state.isLoading = true;
    },
    clearData: () => {
      return initialState;
    },
    loaded: (state, action: PayloadAction<{ data: User }>) => {
      state.user = action.payload.data;
      state.isLoading = false;
    },
    loadError: (state) => {
      state.error = { message: 'Something went wrong' };
      state.isLoading = false;
    },
  },
});

export const { actions } = slice;
export type SliceAction = typeof actions;
export default slice.reducer;
