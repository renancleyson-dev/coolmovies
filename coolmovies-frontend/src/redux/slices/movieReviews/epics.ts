import { gql } from '@apollo/client';
import { Epic, StateObservable } from 'redux-observable';
import { Observable } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { RootState } from '../../store';
import { EpicDependencies } from '../../types';
import { actions, CurrentMovie, MovieReviewView, SliceAction } from './slice';

export const saveMovieReview: Epic = (
  action$: Observable<SliceAction['create']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.create.match),
    switchMap(async ({ payload }) => {
      try {
        const id = payload.data.id;
        await client.mutate({
          mutation: id ? PatchMovieReview : SaveMovieReview,
          variables: payload.data,
          refetchQueries: [
            { query: AllMovieReviewsByIdQuery, variables: { movieId: payload.data.movieId } },
          ],
          awaitRefetchQueries: true,
        });

        return actions.createSuccess();
      } catch (err) {
        console.log(err);
        return actions.createFail();
      }
    })
  );

export const fetchMovieReviews: Epic = (
  action$: Observable<SliceAction['fetch']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetch.match),
    switchMap(async ({ payload }) => {
      try {
        const result: {
          data: {
            allMovieReviews: { nodes: MovieReviewView[] };
            movieById: CurrentMovie;
          };
        } = await client.query({
          query: AllMovieReviewsByIdQuery,
          variables: { movieId: payload.movieId },
        });

        return actions.loaded({
          data: result.data.allMovieReviews.nodes,
          currentMovie: result.data.movieById,
        });
      } catch (err) {
        return actions.loadError();
      }
    })
  );

const SaveMovieReview = gql`
  mutation SaveMovieReview(
    $title: String!
    $body: String!
    $rating: Int!
    $movieId: UUID!
    $userReviewerId: UUID!
  ) {
    createMovieReview(
      input: {
        movieReview: {
          title: $title
          body: $body
          rating: $rating
          movieId: $movieId
          userReviewerId: $userReviewerId
        }
      }
    ) {
      movieReview {
        id
        title
        body
        rating
        userByUserReviewerId {
          name
        }
      }
    }
  }
`;

const PatchMovieReview = gql`
  mutation PatchMovieReview($id: UUID!, $title: String!, $body: String!, $rating: Int!) {
    updateMovieReviewById(
      input: { movieReviewPatch: { title: $title, body: $body, rating: $rating }, id: $id }
    ) {
      movieReview {
        id
        title
        body
        rating
        userByUserReviewerId {
          name
        }
      }
    }
  }
`;

const AllMovieReviewsByIdQuery = gql`
  query AllMovieReviewsByIdQuery($movieId: UUID!) {
    allMovieReviews(filter: { movieId: { equalTo: $movieId } }, orderBy: RATING_DESC) {
      nodes {
        id
        title
        body
        rating
        userByUserReviewerId {
          name
        }
      }
    }
    movieById(id: $movieId) {
      title
      imgUrl
    }
  }
`;
