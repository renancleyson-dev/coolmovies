import React, { FC } from 'react';
import { css } from '@emotion/react';
import {
  Avatar,
  Card,
  CardContent,
  CardHeader,
  Grid,
  IconButton,
  Rating,
  Skeleton,
  Typography,
  useMediaQuery,
} from '@mui/material';
import Image from 'next/image';
import { yellow } from '@mui/material/colors';
import { MovieReviewView } from '../../../../redux/slices/movieReviews/slice';

type Props = {
  movieReviews: (MovieReviewView | undefined)[];
  editReview: (reviewId: string) => void;
};

const MovieReviewsList: FC<Props> = ({ movieReviews, editReview }) => {
  const isLargerThan490 = useMediaQuery('@media (min-width: 490px)');

  const list = movieReviews.map((item, index) => (
    <Grid key={item?.id || index} item xs={isLargerThan490 ? 4 : 12} css={styles.cardContainer}>
      <Card css={styles.card}>
        <CardHeader
          title={
            item ? (
              <Typography css={styles.userName}>{item.userByUserReviewerId.name}</Typography>
            ) : (
              <Skeleton width={'25%'} />
            )
          }
          subheader={
            item ? (
              <Rating readOnly value={item.rating} />
            ) : (
              <Skeleton variant={'rectangular'} width={'40%'} />
            )
          }
          avatar={
            item ? (
              <Avatar
                aria-label={'Review Author'}
                sx={({ palette }) => ({
                  backgroundColor: palette.secondary.main,
                })}
                css={styles.avatar}
              >
                {item.userByUserReviewerId.name.at(0)}
              </Avatar>
            ) : (
              <Skeleton variant={'circular'} width={'50px'} height={'50px'} />
            )
          }
          action={
            item && (
              <IconButton onClick={() => editReview(item.id)}>
                <Image width={16} height={16} src={'/edit.svg'} alt={'edit movie review'} />
              </IconButton>
            )
          }
        />
        <CardContent>
          {item ? (
            <Typography variant={'h6'} component={'div'}>
              {item.title}
            </Typography>
          ) : (
            <Skeleton width={'40%'} height={'40px'} />
          )}
          {item ? (
            <Typography variant={'body2'} component={'div'}>
              {item.body}
            </Typography>
          ) : (
            <Skeleton width={'100%'} height={'140px'} />
          )}
        </CardContent>
      </Card>
    </Grid>
  ));

  if (list.length === 0) {
    return (
      <Typography css={styles.emptyListText}>{'No reviews for this movie were found.'}</Typography>
    );
  }

  return (
    <Grid container css={styles.listContainer} spacing={5}>
      {list}
    </Grid>
  );
};

const styles = {
  listContainer: css({
    marginTop: '0px',
  }),
  cardContainer: css({
    '@media (min-width: 490px)': {
      minWidth: '450px',
    },
  }),
  card: css({ height: '100%' }),
  emptyListText: css({ marginTop: '30px', fontSize: '1.2rem' }),
  filledStar: css({ color: yellow[500] }),
  userName: css({ fontSize: '1.2rem' }),
  avatar: css({ width: '50px', height: '50px', fontSize: '1.3rem' }),
};

export default MovieReviewsList;
